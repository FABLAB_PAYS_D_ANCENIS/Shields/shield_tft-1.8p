#ifndef CBUTTONFTF_H
#define CBUTTONFTF_H

#include "generic.h"

#define cBpRight 0
#define cBpLeft	 1
#define cBpUp    3
#define cBpDown	 4

class CButtonsTft
{
public:
	CButtonsTft();
	bool getValue(uint8 u8Button);
	bool getEdgeValue(uint8 u8Button);

private:
	uint8 u8PinButtonUp;
	uint8 u8PinButtonLeft;
	uint8 u8PinButtonRight;
 uint8 u8PinButtonDown;
	
	uint32 u32TempoBpUp;
	uint32 u32TempoBpDown;
	uint32 u32TempoBpRight;
	uint32 u32TempoBpLeft;
  
	uint32 u32TempoBpUpEdge;
	uint32 u32TempoBpDownEdge;
	uint32 u32TempoBpRightEdge;
	uint32 u32TempoBpLeftEdge;
  
	bool   bBpUpState;
	bool   bBpDownState;
	bool   bBpRightState;
	bool   bBpLeftState;
  
	bool   bBpUpOldState;
	bool   bBpDownOldState;
	bool   bBpRightOldState;
	bool   bBpLeftOldState;
  
	bool   fabDigitalRead(uint8 u8Button);
};

#endif // CBUTTONFTF_H
