#ifndef CTFTSCREEN_H
#define CTFTSCREEN_H

#include "TFT.h"
#include "generic.h"

class CTftScreen : public CTFT {

public:

	void start();  
	void setBackground(uint8_t red, uint8_t green, uint8_t blue);
	void setFill(uint8_t red, uint8_t green, uint8_t blue);
	void setStroke(uint8_t red, uint8_t green, uint8_t blue);
	void setText(double val, int16_t x, int16_t y);
	void setText(const char * val, int16_t x, int16_t y);
 
};


#endif // CTFTSCREEN_H
