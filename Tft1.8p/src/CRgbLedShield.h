#ifndef CRGBLEDSHIELD_H
#define CRGBLEDSHIELD_H

#include "generic.h"
#include "Adafruit_NeoPixel.h"

class CRgbLedShield
{
public:
	CRgbLedShield(uint8 u8PinRgb);
	void switchOnLed(uint8 u8Color);
	void switchOffLed(void);
	void switchOnLedRgb(uint8 u8R, uint8 u8G, uint8 u8B);

private:
	Adafruit_NeoPixel *m_pixels;
};

#endif // CRGBLEDSHIELD_H
