#include "CSystemTft.h"

int cPinRfChipEnable = 8;
int cPinRfChipSelect = 10;
int cPinRfMiso = 12;
int cPinRfMosi = 11;
int cPinRfClock = 13;

/*****************************************************************************
 * Function    : CSystemTft                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CSystemTft::CSystemTft()
{
    // Init pcb rev to 0 = unknown
    //m_u8PcbRevision = 0;
    bErrorDisabled  = false;

    // Detect Pcb
    configure();
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemTft::configure(void)
{
   //m_u8PcbRevision = 1;
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemTft::disableErrors(void)
{
    bErrorDisabled = true;
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemTft::check(void)
{
    // Check if error is disabled
    if(bErrorDisabled == true)
        return;

    // On Rev 1 pcb
    /*if(m_u8PcbRevision == 1)
    {

    }*/
    // On Rev 2 pcb
    /*else if(m_u8PcbRevision == 2)
    {

    }*/
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemTft::error(uint8 u8Err)
{
    if(u8Err == 1)
    {
        
    }
    else if(u8Err == 2)
    {
        
    }

    // Stop processing
    while(1){}
}
