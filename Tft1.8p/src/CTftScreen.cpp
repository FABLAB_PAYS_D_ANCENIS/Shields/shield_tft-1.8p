#include "CTftScreen.h"

/*****************************************************************************
 * Function	: begin                                                          *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CTftScreen::start()
{
	begin();
	background(0,0,0);
	setTextSize(2);
	setFill(50,50,50);
	setStroke(100,100,100);
}

/*****************************************************************************
 * Function	: setBackground                                                  *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CTftScreen::setBackground(uint8_t red, uint8_t green, uint8_t blue)
{
	background(red*255/100,green*255/100,blue*255/100);
}

/*****************************************************************************
 * Function	: setFill                                                        *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CTftScreen::setFill(uint8_t red, uint8_t green, uint8_t blue)
{
	fill(red*255/100,green*255/100,blue*255/100);
}

/*****************************************************************************
 * Function	: setStroke                                                      *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CTftScreen::setStroke(uint8_t red, uint8_t green, uint8_t blue)
{
	stroke(red*255/100,green*255/100,blue*255/100);
}

/*****************************************************************************
 * Function	: text                                                           *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CTftScreen::setText(double val, int16_t x, int16_t y)
{
	String strText = String(int32(val));  
	text(strText.c_str(),x,y);
}

/*****************************************************************************
 * Function	: text                                                           *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CTftScreen::setText(const char * val, int16_t x, int16_t y)
{
	text(val,x,y);
}