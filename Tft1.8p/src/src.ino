#include <SPI.h>
#include <SD.h>
#include "CTftScreen.h"
#include <SoftwareSerial.h>
#include "CSystemTft.h"
#include "generic.h"
#include "CRgbLedShield.h"
#include "CButtonsTft.h"
#include "CBuzzer.h"
#include "CRadioFreq.h"
//#include "CWifi.h"


// Classes
CRgbLedShield  *RgbLed;
CButtonsTft    *Button;
CBuzzer        *Buzzer;
CRadioFreq     *RadioFreq;
CSystemTft     *System;
//CWifi          *Wifi;
CTftScreen *TFTscreen;

// Variables
uint32 u32Count;
uint32 u32Tempo;
uint32 u32Tmp;
uint32 u32Counter;
bool   bOldBpUp;
bool   bOldBpDown;
bool   bOldBpRight;
bool   bOldBpLeft;
uint8  u8BpType;
uint8  u8OldBpType;
uint8  u8MainStep;
uint8  u8AuxStep;
uint8  u8ColorR, u8ColorG, u8ColorB;

// Enums
enum {cStepMenu=0,
      cStepLedRgb,
      cStepBuzzer,
      cStepBp,
      cStepRfTx,
      cStepRfRx,
      cStepWifi
      };



/*Luckily, it’s an easy fix, you only need to make one minor change to one of the library files. Find this file: arduino-1.0.5\libraries\TFT\utility\Adafruit_GFX.cpp

Line 478 should read:

uint16_t Adafruit_GFX::newColor(uint8_t r, uint8_t g, uint8_t b)

change to:

uint16_t Adafruit_GFX::newColor(uint8_t b, uint8_t g, uint8_t r)*/

#define sd_cs  2
#define lcd_cs 5
#define dc     4
#define rst    A2

PImage logo;

/*****************************************************************************
 * Function    : setup                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void setup()
{
    // Serial
    Serial.begin(9600);

    // Instanciate classes
    System      = new CSystemTft();
    RgbLed      = new CRgbLedShield(A3);
    Button      = new CButtonsTft();
    Buzzer      = new CBuzzer(9);
	  TFTscreen   = new CTftScreen();
    RadioFreq   = new CRadioFreq(8,10);
    //Wifi        = new CWifi(7,6);

    System->configure();
    System->disableErrors();

    // Init variables
    u32Count   = 0;
    bOldBpUp   = 0;
    u8MainStep = cStepMenu;
    u8AuxStep  = 0;
    u32Counter = 0;
    u32Tempo   = millis();
    u8OldBpType = 0;        

    // initialize the display
    TFTscreen->begin();
    //TFTscreen->invertDisplay(1);
    // black background
    TFTscreen->background(0, 0, 0);
    TFTscreen->fill(255, 255, 255);
    TFTscreen->stroke(255,255,255);
    // write text to the screen in the top left corner
    TFTscreen->setTextSize(2);
    TFTscreen->text(" Test Shield", 0, 5);
    TFTscreen->text(" - Led RGB", 0, 25);
    TFTscreen->text(" - Buzzer", 0, 45);
    TFTscreen->text(" - Boutons", 0, 65);
    TFTscreen->text(" - Radio Freq", 0, 85);
    TFTscreen->text(" - Wifi", 0, 105);

    /*Serial.print(F("Initializing SD card..."));
  if (!SD.begin(sd_cs)) {
    Serial.println(F("failed!"));
    return;
  }
  Serial.println(F("OK!"));

  // initialize and clear the GLCD screen
  TFTscreen->begin();
  TFTscreen->background(255, 255, 255);

  // now that the SD card can be access, try to load the
  // image file.
  logo = TFTscreen->loadImage("logofab.bmp");
  if (!logo.isValid()) {
    Serial.println(F("error while loading arduino.bmp"));
  }
    TFTscreen->image(logo, 10, 30);*/
}

/*****************************************************************************
 * Function    : loop                                                        *
 *...........................................................................*
 * Description : The loop function runs over and over again forever          *
 *****************************************************************************/
void loop()
{
    int32  s32Tmp;
    int16  s16Tmp;
    uint32 u32Tmp;
    uint8  u8Tmp, u8Bp;

    System->check();

    // Step machine
    switch(u8MainStep)
    {
        case cStepMenu:

           

        break;
        
        /* ------------------------  Leds RGB -----------------------------*/
        case cStepLedRgb:

            if(u8AuxStep == 0)
           {
                Serial.println(F("Led RGB"));
                u8AuxStep = 1;
                TFTscreen->background(0, 0, 0);
                TFTscreen->text(" Led RGB", 0, 5);
                TFTscreen->stroke(0,0,0);
           }
           else
           {
            if((millis() - u32Tempo) > 500)
            {
                TFTscreen->fill(u8ColorR>0?255:0,u8ColorG>0?255:0,u8ColorB>0?255:0);
                TFTscreen->circle(TFTscreen->width()/2, TFTscreen->height()/2, 10);
                RgbLed->switchOnLedRgb(u8ColorR, u8ColorG, u8ColorB);

                //if(u8AuxStep < 9)
                //{
                //   u8AuxStep++;
                //}
                //else
                {
                  // u8AuxStep = 1;
                   u8ColorR  = random(0,2)*10;
                   u8ColorG  = random(0,2)*10;
                   u8ColorB  = random(0,2)*10;

                   while(u8ColorR == 0 && u8ColorG == 0 && u8ColorB == 0)
                   {
                       u8ColorR  = random(0,2)*10;
                       u8ColorG  = random(0,2)*10;
                       u8ColorB  = random(0,2)*10;
                   }
                }

                u32Tempo = millis();
            }
           }
        break;

        /* -------------------------  Buzzer -----------------------------*/
        case cStepBuzzer:

           if(u8AuxStep == 0)
           {
                Serial.println(F(" Buzzer"));
                u8AuxStep = 1;
                TFTscreen->background(0, 0, 0);
                TFTscreen->fill(0, 0, 0);
                TFTscreen->stroke(255,255,255);
                TFTscreen->text(" Buzzer", 0, 5);
           }
           else
           {
              if((millis() - u32Tempo) > 300)
              {
                u8Tmp = random(0,6);
                Buzzer->playNote(u8Tmp);
                
                if(u8Tmp == 0){
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,50,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" DO", 50, 50);}
                else if(u8Tmp == 1){
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,50,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" RE", 50, 50);}
                else if(u8Tmp == 2){
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,50,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" MI", 50, 50);}
                else if(u8Tmp == 3){
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,50,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" FA", 50, 50);}
                else if(u8Tmp == 4){
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,50,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" SOL", 50, 50);}
                else if(u8Tmp == 5){
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,50,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" LA", 50, 50);}
                else if(u8Tmp == 6){
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,50,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" SI", 50, 50);}
                  
                u32Tempo = millis();
              }
           }
        break;

        /* -----------------------  Buttons ------------------------------*/
        case cStepBp:

            if(u8AuxStep == 0)
           {
                Serial.println(F(" Boutons"));
                u8AuxStep = 1;
                TFTscreen->background(0, 0, 0);
                TFTscreen->fill(0, 0, 0);
                TFTscreen->stroke(255,255,255);
                TFTscreen->text(" Boutons", 0, 5);
           }
           else
           {
              if((Button->getEdgeValue(cBpUp) == 1))
              {
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,70,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text("Haut", 50, 50);
                  u8BpType = 1;
              }
              /*else if((Button->getEdgeValue(cBpDown) == 1))
              {
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,70,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text("Bas", 50, 50);
                  u8BpType = 2;
              }*/
              else if((Button->getEdgeValue(cBpRight) == 1))
              {
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,70,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text("Droit", 50, 50);
                  u8BpType = 3;
              }
              else if((Button->getEdgeValue(cBpLeft) == 1))
              {
                  TFTscreen->stroke(0,0,0);
                  TFTscreen->rect(50,50,70,20);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text("Gauche", 50, 50);
                  u8BpType = 4;
              }
              if(u8BpType != u8OldBpType)
              {
  
              }
              u8OldBpType = u8BpType;
            }
            

        break;

        /* ------------------  Radio-frequency ---------------------------*/
        case cStepRfTx:
            if(u8AuxStep == 0)
            {
                if((millis() - u32Tempo) > 700)
                {
                    u8AuxStep = 1;
                    RadioFreq->init(1);
                    RadioFreq->destAddress(0);

                    s16Tmp = random(0,10999)-1000;
                    TFTscreen->background(0, 0, 0);
                    TFTscreen->fill(0, 0, 0);
                    TFTscreen->stroke(255,255,255);
                    TFTscreen->text(" RF Tx", 0, 5);

                    RadioFreq->send(s16Tmp, 1);
                    u32Tempo = millis();
                }
            }
            else
            {
                if((millis() - u32Tempo) > 3000)
                {
                    s16Tmp = random(0,10999)-1000;
                    TFTscreen->stroke(0,0,0);
                    TFTscreen->rect(50,50,70,20);
                    TFTscreen->stroke(255,255,255);
                    TFTscreen->setText(s16Tmp, 50, 50);

                    RadioFreq->send(s16Tmp, 0);
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------  Radio-frequency ---------------------------*/
        case cStepRfRx:
            if(u8AuxStep == 0)
            {
                
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;
                  TFTscreen->background(0, 0, 0);
                  TFTscreen->fill(0, 0, 0);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" RF Rx", 0, 5);

                  RadioFreq->init(0);
                  RadioFreq->destAddress(1);
                }
            }
            else
            {
                if((millis() - u32Tempo) > 500)
                {
                    if(RadioFreq->isCodeReceived())
                    {
                      s32Tmp = RadioFreq->receiveCode();
                      TFTscreen->stroke(0,0,0);
                      TFTscreen->rect(50,50,70,20);
                      TFTscreen->stroke(255,255,255);
                      TFTscreen->setText((double)(s32Tmp), 50, 50);

                    }
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------  Wifi ---------------------------*/
        case cStepWifi:
            if(u8AuxStep == 0)
            {
                u8AuxStep = 1;
                TFTscreen->background(0, 0, 0);
                  TFTscreen->fill(0, 0, 0);
                  TFTscreen->stroke(255,255,255);
                  TFTscreen->text(" RF Rx", 0, 5);


                //Wifi->connect((char*)(" "), (char*)(" "));


                //Wifi->loginFree((char*)(" "), (char*)(" "));
            }
            else
            {
                if(Button->getValue(cBpDown) == 1)
                {
                   u8AuxStep++;
                   //Wifi->send(u8AuxStep);
                   //Wifi->sendSms((char*)("Hello !"));
                }
             }
        break;

        /* ------------------------- Default ----------------------------*/
        default:
            // Init variables
            u32Count = 0;
            bOldBpUp = 0;
            u8MainStep = cStepMenu;
            u8AuxStep = 0;
            u32Tmp = 0;
            u32Tempo = millis();
            //RadioFreq->stop();
        break;
    }

    /* ---------------- Increment step machine ------------------------*/
    if((Button->getEdgeValue(cBpDown) == 1))
    {
        u8MainStep++;
        u32Tempo = millis();
        u32Tmp = 0;
        u8AuxStep = 0;

        // Stop all devices
        RgbLed->switchOffLed();
        Buzzer->stopNote();
        
    }
    bOldBpUp    = Button->getValue(cBpUp);
    bOldBpDown  = Button->getValue(cBpDown);
    bOldBpRight = Button->getValue(cBpRight);
    bOldBpLeft  = Button->getValue(cBpLeft);
}
