#include "CRgbLedShield.h"

/*****************************************************************************
 * Function		: CRgbLedShield                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CRgbLedShield::CRgbLedShield(uint8 u8PinRgb)
{
	// When we setup the NeoPixel library, we tell it how many m_pixels, and which pin to use to send signals.
	m_pixels = new Adafruit_NeoPixel(1, u8PinRgb, NEO_GRB + NEO_KHZ800); 
	m_pixels->begin();
	// Shutdown all leds.
	m_pixels->setPixelColor(0, m_pixels->Color(0,0,0)); 
	// This sends the updated pixel color to the hardware.
	m_pixels->show(); 
}

/*****************************************************************************
 * Function		: switchOnLed                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLedShield::switchOnLed(uint8 u8Color)
{
	/*
	List of colors availables
	color == 0 -> Red
	color == 1 -> Orange
	
	color == 2 -> Green
	color == 3 -> Yellow
	color == 4 -> Blue
	color == 5 -> Indigo
	color == 6 -> Purple
	color == "other" -> White
	*/

	if (u8Color == 0)
	{
		m_pixels->setPixelColor(0, m_pixels->Color(10,0,0)); // Setcolor to red.
	}
	else if (u8Color == 1)
	{
		m_pixels->setPixelColor(0, m_pixels->Color(10,5,0)); // Setcolor to orange.
	}
	else if (u8Color == 2)
	{
		m_pixels->setPixelColor(0, m_pixels->Color(0,10,0)); // Setcolor to green.
	}
	else if (u8Color == 3)
	{
		m_pixels->setPixelColor(0, m_pixels->Color(10,10,0)); // Setcolor to yellow.
	}
	else if (u8Color == 4)
	{
	 m_pixels->setPixelColor(0, m_pixels->Color(0,0,10)); // Setcolor to blue.
	}
	else if (u8Color == 5)
	{
		m_pixels->setPixelColor(0, m_pixels->Color(5,0,10)); // Setcolor to indigo.
	}
	else if (u8Color == 6)
	{
		m_pixels->setPixelColor(0, m_pixels->Color(10,0,10)); // Setcolor to purple.
	}
	else
	{
		m_pixels->setPixelColor(0, m_pixels->Color(10,10,10)); // Setcolor to white.
	}

	// This sends the updated pixel color to the hardware.
	m_pixels->show(); 
}

/*****************************************************************************
 * Function		: switchOffLed                                               *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLedShield::switchOffLed(void)
{
	// Shutdown led.
	m_pixels->setPixelColor(0, m_pixels->Color(0,0,0)); 
	// This sends the updated pixel color to the hardware.
	m_pixels->show(); 
}

/*****************************************************************************
 * Function		: switchOnLedRgb                                             *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLedShield::switchOnLedRgb(uint8 u8R, uint8 u8G, uint8 u8B)
{
	// Setcolor to specific color.
	m_pixels->setPixelColor(0, m_pixels->Color(u8R,u8G,u8B)); 
	// This sends the updated pixel color to the hardware.
	m_pixels->show();
}

