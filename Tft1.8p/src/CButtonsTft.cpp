#include "CButtonsTft.h"

/*****************************************************************************
 * Function	: CButtonsTft                                                        *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CButtonsTft::CButtonsTft()
{
	// Setup pin
	u8PinButtonUp    = 3;
	u8PinButtonLeft  = A0;
	u8PinButtonRight = A1;
	u8PinButtonDown  = 0;

	pinMode(u8PinButtonUp,   INPUT_PULLUP);
	pinMode(u8PinButtonLeft, INPUT_PULLUP);
	pinMode(u8PinButtonRight,INPUT_PULLUP);
	pinMode(u8PinButtonDown, INPUT_PULLUP);

	// Init tempos
	u32TempoBpUp	= 0;
	u32TempoBpDown  = 0;
	u32TempoBpRight = 0;
	u32TempoBpLeft  = 0;

	u32TempoBpUpEdge	= 0;
	u32TempoBpDownEdge  = 0;
	u32TempoBpRightEdge = 0;
	u32TempoBpLeftEdge  = 0;

	// Init states
	bBpUpState	  = fabDigitalRead(cBpUp);
	bBpDownState  = fabDigitalRead(cBpDown);
	bBpRightState = fabDigitalRead(cBpRight);
	bBpLeftState  = fabDigitalRead(cBpLeft);

	  // Init old state
	bBpUpOldState	 = 0;
	bBpDownOldState  = 0;
	bBpRightOldState = 0;
	bBpLeftOldState  = 0;
}

/*****************************************************************************
 * Function	: getValue                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButtonsTft::getEdgeValue(uint8 u8Button)
{
	bool bRes = 0;

	if(u8Button == cBpUp)
	{
		if((millis() - u32TempoBpUpEdge) > 100)
		{
			bRes = fabDigitalRead(cBpUp);

			if(bBpUpOldState != bRes)
			{
				u32TempoBpUpEdge = millis();

						if(bRes > bBpUpOldState)
						{
							bBpUpOldState = bRes;
							return 1;
						}
			}

				  bBpUpOldState = bRes;

			return 0;
		}
		else
		{
			return 0;
		}
	}
	else if(u8Button == cBpDown)
	{
		if((millis() - u32TempoBpDownEdge) > 100)
		{
			bRes = fabDigitalRead(cBpDown);

			if(bBpDownOldState != bRes)
			{
				u32TempoBpDownEdge = millis();

						if(bRes > bBpDownOldState)
						{
							bBpDownOldState = bRes;
							return 1;
						}
			}

				  bBpDownOldState = bRes;

			return 0;
		}
		else
		{
			return 0;
		}
	}
	else if(u8Button == cBpRight)
	{
		if((millis() - u32TempoBpRightEdge) > 100)
		{
			bRes = fabDigitalRead(cBpRight);

			if(bBpRightOldState != bRes)
			{
				u32TempoBpRightEdge = millis();

						if(bRes > bBpRightOldState)
						{
							bBpRightOldState = bRes;
							return 1;
						}
			}

				  bBpRightOldState = bRes;

			return 0;
		}
		else
		{
			return 0;
		}
	}
	else if(u8Button == cBpLeft)
	{
		if((millis() - u32TempoBpLeftEdge) > 100)
		{
				  bRes = fabDigitalRead(cBpLeft);

			if(bBpLeftOldState != bRes)
			{
				u32TempoBpLeftEdge = millis();

						if(bRes > bBpLeftOldState)
						{
							bBpLeftOldState = bRes;
							return 1;
						}
			}

				  bBpLeftOldState = bRes;

			return 0;
		}
		else
		{
			return 0;
		}
	}

	return bRes;
}

/*****************************************************************************
 * Function	: getValue                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButtonsTft::getValue(uint8 u8Button)
{
	bool bRes = 0;

	if(u8Button == cBpUp)
	{
		if((millis() - u32TempoBpUp) > 100)
		{
			//Serial.println(bBpUpState);
			if(bBpUpState != fabDigitalRead(cBpUp))
			{
				bBpUpState = fabDigitalRead(cBpUp);
				u32TempoBpUp = millis();
			}

			return bBpUpState;
		}
		else
		{
			return bBpUpState;
		}
	}
	else if(u8Button == cBpDown)
	{
		if((millis() - u32TempoBpDown) > 100)
		{
			if(bBpDownState != fabDigitalRead(cBpDown))
			{
				bBpDownState = fabDigitalRead(cBpDown);
				u32TempoBpDown = millis();
			}

			return bBpDownState;
		}
		else
		{
			return bBpDownState;
		}
	}
	else if(u8Button == cBpRight)
	{
		if((millis() - u32TempoBpRight) > 100)
		{

			if(bBpRightState != fabDigitalRead(cBpRight))
			{
				bBpRightState = fabDigitalRead(cBpRight);
				u32TempoBpRight = millis();
			}

			return bBpRightState;
		}
		else
		{
			return bBpRightState;
		}
	}
	else if(u8Button == cBpLeft)
	{
		if((millis() - u32TempoBpLeft) > 100)
		{

			if(bBpLeftState != fabDigitalRead(cBpLeft))
			{
				bBpLeftState = fabDigitalRead(cBpLeft);
				u32TempoBpLeft = millis();
			}

			return bBpLeftState;
		}
		else
		{
			return bBpLeftState;
		}
	}

	return bRes;
}

/*****************************************************************************
 * Function	: fabDigitalRead                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButtonsTft::fabDigitalRead(uint8 u8Button)
{
	uint16 u16Val;

	if(u8Button == cBpUp)
	{
		return digitalRead(u8PinButtonUp) == 0;
	}
	if(u8Button == cBpLeft)
	{
		return digitalRead(u8PinButtonLeft) == 0;
	}
	if(u8Button == cBpRight)
	{
		return digitalRead(u8PinButtonRight) == 0;
	}
	if(u8Button == cBpDown)
	{
		return digitalRead(u8PinButtonDown) == 0;
	}
}
